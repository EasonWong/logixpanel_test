<?php

namespace App\Listeners;

use App\Events\Contracts\IUsersEvent;
use App\Events\UsersCreateEvent;

class EventsListener
{
    private const EVENT_USER_CREATED = 'UsersCreateEvent';
    private const EVENT_USER_DELETED = 'UsersDeleteEvent';
    private const EVENT_USER_UPDATED = 'UsersUpdateEvent';

    public function handle(IUsersEvent $event)
    {   
        $class = get_class($event);
        switch($class)
        {
            case self::EVENT_USER_CREATED:
                //send create email
                break;
            case self::EVENT_USER_UPDATED:
                //send update email
                break;
            case self::EVENT_USER_DELETED:
                //send delete email
                break;
        }
    }
}
