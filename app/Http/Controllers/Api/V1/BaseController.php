<?php

namespace App\Http\Controllers\Api\V1;


use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    use ValidatesRequests;
}
