<?php

namespace App\Http\Controllers\Api\V1;

use App\Services\Contracts\IUsersService;

class UsersController extends BaseController
{
    private $userService;

    public function __construct(IUsersService $userService)
    {
        $this->userService = $userService;
    }

    public function store()
    {
        return $this->userService->createFakeUser();
    }

    
    public function destroy($uId)
    {
        $result = $this->userService->deleteUserById($uId);
        if ($result) {
            return response()->json(
                [
                    "error" => "Resource not found."
                ],
                404
            );
        }
        return response()->json(
            [
                "message" => "Resource deleted successfully."
            ],
            200
        );
    }

    
}