<?php

namespace App\Repositories;

use App\Repositories\Contracts\IUsersRepo;
use App\Models\Users;
use Illuminate\Database\Eloquent\Model;

class UsersRepoImpl implements IUsersRepo
{
    private $model;
    public function __construct(Users $user)
    {
        $this->model = $user;
    }

    //CRUD
    public function getById($nId): ?Model
    {
        return $this->model->find($nId);
    }

    public function create(array $data): ?Model
    {
        return $this->model->create($data);
    }

    public function delete($uId): bool
    {
        return $this->model->where('id', $uId)->delete();
    }

    public function update($uId, array $data): bool
    {
        return $this->model->where('id', $uId)->update($data);
    }


}
