<?php

namespace App\Repositories\Contracts;

use Illuminate\Database\Eloquent\Model;

interface IRepo
{
    //CURD
    public function getById($nId): ?Model;
    public function create(array $data): ?Model;
    public function delete($uid): bool;
    public function update($uid, array $data): bool;
}
