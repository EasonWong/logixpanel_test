<?php

namespace App\Events;

use App\Events\Contracts\IUsersEvent;
use App\Models\Users;

class UsersCreateEvent implements IUsersEvent
{
 

    private $user;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Users $user)
    {
        $this->user = $user;
    }
}
