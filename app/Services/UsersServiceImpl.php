<?php

namespace App\Services;

use App\Repositories\Contracts\IUsersRepo;
use Illuminate\Database\Eloquent\Model;
use App\Services\Contracts\IUsersService;
use Illuminate\Support\Str;

class UsersServiceImpl implements IUsersService
{
    private $usersRepo;
    public function __construct(IUsersRepo $usersRepo) {
        $this->usersRepo = $usersRepo;
    }



    public function createFakeUser(): ?Model
    {
        $data = [
            'name' => fake()->name(),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
            'remember_token' => Str::random(10),
        ];

        return $this->usersRepo->create($data);
    }

    public function deleteUserById($uId): bool
    {
        return $this->usersRepo->delete($uId);
    }
}