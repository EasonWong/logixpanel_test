<?php

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Model;

interface IUsersService
{
    public function createFakeUser(): ?Model; 

    public function deleteUserById($uId): bool;
}
