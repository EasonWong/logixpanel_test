<?php
/**
 * Created by PhpStorm.
 * User: ShawnWang
 * Date: 10/05/2018
 * Time: 10:14 AM
 */

namespace App\Observers;

use App\Events\UsersCreateEvent;
use App\Events\UsersDeleteEvent;
use App\Events\UsersUpdateEvent;
use App\Models\Users;
use Psr\Log\LoggerInterface;

class UsersObserver
{
    private $logger;
    public function __construct()
    {
        $this->logger = app(LoggerInterface::class);
    }

    public function created(Users $model)
    {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        event(new UsersCreateEvent($model));
    }

    public function saved(Users $model)
    {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        event(new UsersUpdateEvent($model));
    }

    public function deleted(Users $model)
    {
        $this->logger->info(__CLASS__.' '.__FUNCTION__);
        event(new UsersDeleteEvent($model));
    }


}