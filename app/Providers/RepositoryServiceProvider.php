<?php

namespace App\Providers;

use App\Repositories\Contracts\IUsersRepo;
use App\Repositories\UsersRepoImpl;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider  extends ServiceProvider
{
    
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(IUsersRepo::class, UsersRepoImpl::class);
    }
}
