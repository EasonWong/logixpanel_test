<?php

namespace App\Providers;

use App\Models\Users;
use App\Observers\UsersObserver;
use App\Services\Contracts\IUsersService;
use App\Services\UsersServiceImpl;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Users::observe(UsersObserver::class);
        $this->app->bind(IUsersService::class, UsersServiceImpl::class);
    }
}
